def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, reps = 2)
  rep_array = []
  reps.times { rep_array << str }
  rep_array.join(" ")
end

def start_of_word(str, num_letters = 1)
  str[0..num_letters - 1]
end

def first_word(str)
  str[0...str.index(" ")]
end

def titleize(str)
  little_words = ["the", "a", "at", "by", "of", "over", "and"]
  words = str.split
  words.map!.with_index do |word, idx|
    if idx == 0
      word.capitalize
    elsif little_words.include?(word)
      word
    else
      word.capitalize
    end
  end

  words.join(" ")
end
