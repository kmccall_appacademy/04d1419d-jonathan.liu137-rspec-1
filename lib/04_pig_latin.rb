def translate(str)
  vowels = "aeiouAEIOU"
  words = str.split
  translated_words = []
  words.each do |word|
    if vowels.include?(word[0])
      translated_words << word + "ay"
    elsif word == word.capitalize
      rotated = word.chars.rotate(find_new_start(word)).join("")
      translated_words << rotated.downcase.capitalize + "ay"
    else
      rotated = word.chars.rotate(find_new_start(word)).join("")
      translated_words << rotated + "ay"
    end
  end

  translated_words.join(" ")
end

def find_new_start(word)
  new_start = word.index(/[aeiou]/)
  if word[new_start] == 'u' && word[new_start - 1] == 'q'
    return new_start + 1
  else
    return new_start
  end
end
