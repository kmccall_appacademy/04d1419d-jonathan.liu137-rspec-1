def ftoc(f_degrees)
  5.0 / 9 * (f_degrees - 32)
end

def ctof(c_degrees)
  9.0 / 5 * c_degrees + 32
end
